Clinical Renal Associates January 12, 2018
860 Springdale Drive Suite 100 Exton, PA 19341 Page 4
610-524-3703 Fax: 610-524-5990 Office Visit

Home: (610) 918-0976
Female DOB: 11/25/1950 64721 Ins: OTHER (OTHER) Grp: 5755221501

General:
well developed, well nourished, in no acute distress
Eyes:
PERRLAJEOM intact; fundi benign, conjunctiva and sclera clear
Mouth:
no deformity or lesions with good dentition
Neck:
no masses, thyromegaly, or abnormal cervical nodes
Lungs:
clear bilaterally to A& P
Heart:
regular rate and rhythm, S1, S2 without murmurs, rubs, gallops, or clicks
Abdomen:
bowel sounds positive; abdomen soft and non-tender without masses, organomegaly, or hernias noted
Extremities:
no clubbing, cyanosis, edema, or deformity noted with normal full range of motion of all joints
Neurologic:
no focal deficits, CN II-XI grossly intact with normal reflexes, coordination, muscle strength and tone
Skin:
intact without lesions or rashes
Psych:
alert and cooperative; normal mood and affect; normal attention span and concentration

Renal Evaluation:

Tests Reviewed:

BUN: 13 05/12/2017
Creatinine: 0.82 05/12/2017
Sodium: 142 05/12/2017
Potassium: 4.2 05/12/2017
Chloride: 106 05/12/2017
Cco2: 25 05/28/2015
HGB: 14.9 05/12/2017
HCT: 46.2 05/12/2017
MCV: 96.4 05/12/2017
MCH: 31.2 05/12/2017
Platelets:
249 THOUSAND/UL (05/12/2017)

ESR: 6 02/29/2016

New Orders:

1) SNOMED-CT: 308335008: Patient encounter procedure (procedure) (SCT-
308335008)

2) CBC without Diff (Hemogram) (1759QHO) (028142LCA) (CPT-85027)

3) Renal Function Panel WITH eGFR (10314QHO) (322777LCA) (CPT-80069)
4) Tacrolimus FK506 (34482QHO) (700248LCA) (CPT-80197)

Impression & Recommendations:

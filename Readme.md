- Other Dependencies are mention in req.txt file.
- Tesseract install with languages and leptonica Tesseract Installing Dependencies

        $ sudo apt-get install libpng-dev libjpeg-dev libtiff-dev zlib1g-dev
        $ sudo apt-get install gcc g++
        $ sudo apt-get install autoconf automake libtool checkinstall



We need image processing toolkit Leptonica to build Tesseract.
During the writing of this tutorial, the latest version of Leptonica was 1.78.0,
please check Leptonica’s official website for the latest version. Installing the latest version is highly recommended.

    $ cd ~
    $ wget http://www.leptonica.org/source/leptonica-1.78.0.tar.gz
    $ tar -zxvf leptonica-1.78.0.tar.gz
    $ cd leptonica-1.78.0
    $ ./configure
    $ make
    $ sudo checkinstall
    $ sudo ldconfig

Compiling Tesseract
Now clone Tesseract and build it.

    $ cd ~
    $ git clone https://github.com/tesseract-ocr/tesseract.git
    $ cd tesseract
    $ ./autogen.sh
    $ ./configure
    $ make
    $ sudo make install
    $ sudo ldconfig

Installing Language Components
Now download English language data for the OCR engine:

    $ cd ~
    $ git clone https://github.com/tesseract-ocr/tessdata.git
    $ sudo mv ~/tessdata/* /usr/local/share/tessdata/

Tesserect has finally been installed and configured! Now let’s run it.
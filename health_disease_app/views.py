import os
import sys
import glob
import concurrent.futures
import cv2
import nltk
import numpy as np
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from pdf2image import convert_from_path
import re
from nltk.tokenize import RegexpTokenizer
import pandas as pd
import tempfile
from health_disease import settings
from health_disease_app.utils import text_genrator, custom_neg_words, lookup_list, new_dict
import pytesseract

tokenizer = RegexpTokenizer(r'\w+')

path = settings.MEDIA_ROOT
os.environ['OMP_THREAD_LIMIT'] = '1'


def home(request):
    """

    :return:
    """

    text = ''
    disease_lst = []
    match_txt = ''
    try:
        dir_name = path + '/../'
        print(dir_name)
        test = os.listdir(dir_name)
        for item in test:
            if item.endswith(".jpg"):
                os.remove(os.path.join(dir_name, item))
        files,filename = file_system(request)
        with tempfile.TemporaryDirectory() as path1:
            images_from_path = convert_from_path(files, output_folder=path1)
        for page in images_from_path:
            page.save(path + '/converted_img/converted.jpg', 'JPEG')
            img = cv2.imread(path + '/converted_img/converted.jpg', 0)
            # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            kernel = np.ones((1, 40), np.uint8)
            morphed = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
            # (2) Invert the morphed image, and add to the source image:
            dst = cv2.add(img, (255 - morphed))
            scale_percent = 95  # percent of original size
            width = int(dst.shape[1] * scale_percent / 100)
            height = int(dst.shape[0] * scale_percent / 100)
            dim = (width, height)
            # resize image
            gray_img = cv2.resize(dst, dim, interpolation=cv2.INTER_AREA)
            i = images_from_path.index(page)

            cv2.imwrite('gray{}.jpg'.format(i), gray_img)

        with concurrent.futures.ProcessPoolExecutor(max_workers=4) as executor:
            image_list = glob.glob("*.jpg")
            image_list.sort()
            for img_path, out_file in zip(image_list, executor.map(tesseractt, image_list)):
                text = out_file
                page_no = image_list.index(img_path) + 1
                match_txt1 = ''
                pdf_text = re.finditer(r'(\bInactive Problems\b|\bInactive Probleiis\b|\bInactive Problemis\b)'
                                       r'((.*\n){3})',
                                       text)
                for txt in pdf_text:
                    match_txt1 = txt.group()

                text1 = text.replace(match_txt1, '')
                pdf_text = re.finditer(
                    r'(\bDATE OF SERVICE\b|\bDate of service\b|\bVisit Date:\b|\bVisit Date\b|\bCurrent;\b|\bDOB:\b'
                    r'|\bDO8:\b|\bDOS:\b|\bDate of Visit\b|\bNote Date\b|\bEncounter Date\b)((.*\n){1})',
                    text1)
                for txt in pdf_text:
                    match_txt = txt.group()
                    if 'DO8' in match_txt:
                        match_txt = match_txt.replace('DO8', 'DOS')

                # page_no = images_from_path.index(page) + 1
                genrated_text = text_genrator(text1)
                if match_txt == '':
                    match_txt = "not found"

                disease_lst.append([page_no, match_txt, genrated_text])
        for i in disease_lst:
            if not i[2]:
                disease_lst.remove(i)
            elif type(i) is list:
                for m in custom_neg_words:
                    for j in i[2]:
                        if m in j.split():
                            i[2].remove(j)

        for i in disease_lst:
                for j in lookup_list:
                    if type(i[2]) is list:
                        for k in i[2]:
                            if j in k:
                                i[2][i[2].index(k)] = j
        for page_data in disease_lst:
            if type(page_data[2]) is list:
                page_data[2] = list(set(page_data[2]))
        df = pd.DataFrame(disease_lst)
        page_no = df[0]
        date_of_service = df[1]
        disease = df[2]
        code = []
        for i in disease:
            tmp = []
            for j in i:
                tmp.append(new_dict.get(j.upper()))
            code.append(tmp)
        d_list = zip(page_no, date_of_service, disease, code)
        return render(request, 'home.html', {
            "filename": filename,
            "D_list": d_list,
        })
    except Exception as e:
        print("Exception", e)
    return render(request, 'home.html', {})


def tesseractt(image_file):
    # os.system("tesseract -l eng " + image_file + " out --oem 3 --psm 3")
    os.system("tesseract -l eng " + image_file + " out")
    file = open('out.txt', 'r')
    text = file.read()
    print(text)
    return text


def file_system(request):
    """

    :return:
    """
    if request.method == 'POST':
        myfile = request.FILES['files']
        new_disease = request.POST.get('text')
        if new_disease != '':
            lookup_list.append(new_disease)
        fs = FileSystemStorage()
        if os.path.exists(path + '/' + myfile.name):
            os.remove(path + '/' + myfile.name)
        filename = fs.save(myfile.name, myfile)
        files = path + '/' + filename

        return files,filename


def login(request):
    """

    :return:
    """
    if request.method == 'POST':
        email = request.POST.get('email')
        return render(request, 'home.html', {})

    return render(request, 'login.html', {})

from django.conf.urls.static import static
from health_disease import settings
from django.urls import path
from health_disease_app import views


urlpatterns = [
    path('', views.login, name='login'),
    path('home', views.home, name='home'),
]
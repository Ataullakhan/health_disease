from django.apps import AppConfig


class HealthDiseaseAppConfig(AppConfig):
    name = 'health_disease_app'

from django.db import models

# Create your models here.


class RuleDxCode(models.Model):
    rule_DxCode = models.CharField(max_length=50)
    rule_Text1 = models.CharField(max_length=200)
    rule_Text2 = models.CharField(max_length=200)
    rule_Text3 = models.CharField(max_length=200)
    rule_Text4 = models.CharField(max_length=200)
    rule_Text5 = models.CharField(max_length=200)
    rule_Text6 = models.CharField(max_length=200)
    rule_Text7 = models.CharField(max_length=200)
    rule_Text8 = models.CharField(max_length=200)
    rule_Text9 = models.CharField(max_length=200)
    rule_Text10 = models.CharField(max_length=200)

    class Meta:
        db_table = "rule_dx_code"


class BMIRule(models.Model):
    sr_no = models.IntegerField()
    rule_DxCode = models.CharField(max_length=50)
    rule_Data = models.CharField(max_length=100)
    rule_MinRange = models.IntegerField()
    rule_MaxRange = models.IntegerField()

    class Meta:
        db_table = "bmi_rule"


class DOSLabels(models.Model):
    sr_no = models.IntegerField()
    DOS_Label = models.CharField(max_length=100)

    class Meta:
        db_table = 'dos_labels'


class ExclusionKeyword(models.Model):
    sr_no = models.IntegerField()
    Keyword = models.CharField(max_length=100)

    class Meta:
        db_table = 'exclusion_keyword'


class ReplaceWithBlank(models.Model):
    sr_no = models.IntegerField()
    RepKey_Text = models.CharField(max_length=200)

    class Meta:
        db_table = 'replace_with_blank'


class AcceptNonAcceptHeader(models.Model):
    sr_no = models.IntegerField()
    AcceptHeader = models.CharField(max_length=150)
    NonAcceptHeader = models.CharField(max_length=150)

    class Meta:
        db_table = 'accept_nonaccept_header'


class ProviderCredential(models.Model):
    sr_no = models.IntegerField()
    AcceptCredential = models.CharField(max_length=100)
    NonAcceptCredential = models.CharField(max_length=100)

    class Meta:
        db_table = 'provider_credential'


class IPConditions(models.Model):
    sr_no = models.IntegerField()
    ip_Code = models.CharField(max_length=100)

    class Meta:
        db_table = 'ip_conditions'


class FamilyGrouping(models.Model):
    sr_no = models.IntegerField()
    frr_GroupingDxCode = models.IntegerField()
    frr_DxCode = models.CharField(max_length=100)
    frr_Result = models.CharField(max_length=100)

    class Meta:
        db_table = 'family_grouping'


class DSHeaders(models.Model):
    sr_no = models.IntegerField()
    ds_Keyword = models.CharField(max_length=200)
    ds_Type = models.IntegerField()
    Discharge_Summary_Type = models.CharField(max_length=200)

    class Meta:
        db_table = 'ds_headers'


class MasterTable(models.Model):
    sr_no = models.IntegerField()
    rule_DxCode = models.CharField(max_length=50)
    rule_Text1 = models.CharField(max_length=200)
    rule_Text2 = models.CharField(max_length=200)
    rule_Text3 = models.CharField(max_length=200)
    rule_Text4 = models.CharField(max_length=200)
    rule_Text5 = models.CharField(max_length=200)
    rule_Text6 = models.CharField(max_length=200)
    rule_Text7 = models.CharField(max_length=200)
    rule_Text8 = models.CharField(max_length=200)
    rule_Text9 = models.CharField(max_length=200)
    rule_Text10 = models.CharField(max_length=200)
    BMI_rule_DxCode = models.CharField(max_length=50)
    rule_Data = models.CharField(max_length=100)
    rule_MinRange = models.IntegerField()
    rule_MaxRange = models.IntegerField()
    DOS_Label = models.CharField(max_length=100)
    Keyword = models.CharField(max_length=100)
    RepKey_Text = models.CharField(max_length=200)
    AcceptHeader = models.CharField(max_length=150)
    NonAcceptHeader = models.CharField(max_length=150)
    AcceptCredential = models.CharField(max_length=100)
    NonAcceptCredential = models.CharField(max_length=100)
    ip_Code = models.CharField(max_length=100)
    frr_GroupingDxCode = models.IntegerField()
    frr_DxCode = models.CharField(max_length=100)
    frr_Result = models.CharField(max_length=100)
    ds_Keyword = models.CharField(max_length=200)
    ds_Type = models.IntegerField()
    Discharge_Summary_Type = models.CharField(max_length=200)

    class Meta:
        db_table = 'master_table'
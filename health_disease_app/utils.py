# import math
# from collections import Counter
import string
from nltk.tokenize import RegexpTokenizer
import nltk
import itertools
import os
from health_disease import settings
# import re
tokenizer = RegexpTokenizer(r'\w+')
path = settings.MEDIA_ROOT

custom_neg_words = ['no', 'not', 'normal', 'unable', 'absent', 'absence', 'denies', 'denles', 'without']

lookup_list = ['Hemiparesis', 'Ovarian Cancer', 'Leg swelling', 'fluttering/palpitations',
               'photopsias', 'Presbyopia', 'BMI 45', 'Mitral valve disorder',
               'Morbidly obese','Aortic sclerosis', 'Cardiac arrest',
               'Attention and concentration deficit', 'Asymmetry of optic nerve of both eyes', 'Osteopenia',
                'CHF',
               'Rheumatoid Arthritis', 'Weight Loss', 'Hearing loss', 'Carotid Stenosis',
               'Mild episode of recurrent major depressive disorder',
               'Esophagitis esophagitis','Cardiomyopathy unspecified',
                'Age-related nuclear cataract of both eyes',
                'hematuria', 'Thyromegaly', 'Ovarian cancer', 'hemoptysis', 'Thyroid disorder',
               'Vitamin B12 deficiency', 'First degree hemorrholds', 'Hyperopia',
               'Heart failure Diastolic dysfunction', 'dyslipidemia', 'eye disorder', 'dysuria',
               'Kidney stone', 'AIDS', 'Encounter for screening for other disorder', 'migraines',
               'diastolic heart failure Acute', 'Personal history of nicotine dependence',
                'AML', 'Atrial fibrillation',
               'TIA', 'Gastro-esophageal reflux disease without esophagitis',
               'Headache Primary',
               'Rheumatoid arthritis with rheumatoid factor of multiple sites without organ or systems involvement',
               'Acute on chronic diastolic heart failure', 'Chest Pain', 'Chronic systolic congestive heart failure',
               'Insulin', 'Gastroesophageal reflux disease', 'Interstitial Lung disease', 'Nasal congestion',
               'cough', 'cataracts', 'Paroxysmal Atrial fibrillation', 'conjunctiva', 'hypertension',
               'Congestive heart failure', 'Myocardial infarction', 'Systolic congestive heart failure',
               'Depression','Genitourinary', 'Hypercholesterolemia pure',
               'Diabetic Polyneuropathy', 'Depressive disorder', 'Iron deficiency anemia', 'Sleep disorder', 'AKI',
               'DM Polyneuropathy', 'New palpable lump left breast', 'Rheumatoid arthritis with rheumatoid factor',
               'Leiomyoma of uterus', 'dyspnea', 'CAD Angina', 'diastolic heart failure', 'hepatomegaly',
               'diarrhea-predominant (IBS-D)', 'Colon cancer', 'Angio-edema', 'PVD', 'Floater',
               'diabetes', 'Malignant neoplasm of large intestine', 'Andonimal Pain',
               'Malignant neoplasm of upper-outer quadrant of left female breast', 'metastatic disease',
               'Glaucoma', 'ADD', 'pachymeningitis', 'Acid reflux', 'Dizziness',
               'Primary open angle glaucoma of both eyes', 'Chronic thrombocytopenia', 'Lantus', 'Cancer',
               'Hypercholesterolemia', 'oropharynx and tonsils ', 'Diverticulosls (No Bleed)', 'Snores',
               'spontaneous', 'DM', 'Asthma', 'Vitamin D deficiency', 'Pancreatic Cancer', 'Pulmonary fibrosis',
               'Breast cancer', 'CAD', 'h/o iron defficiency ',
               'wheezing', 'HIV', 'Heart Failure', 'Arthritis', 'diarrhea', 'Sleep apnea', 'Fatigue',
               'abdominal_pain', 'Abbnormal blood sugar', 'UTI symptoms', 'Stage I right breast cancer',
               'Intracranial hypotension', 'Acute maxillary sinusitis', 'hyperlipidemia',
               'Aneurysm', 'peripheral lymphadenopathy', 'Prostatitis ', 'Gallbladder polyp', 'Constipation',
               'Nuclear sclerosis of both eyes', 'Diastolic congestive heart failure', 'Dyslipidemia',
               'APPENDECTOMY', 'Stroke', 'Changes in retinal vascular appearance of both eyes', 'Angina',
               'Aortic aneurysm', 'Dementia', 'Urticaria', 'High Blood Pressure', 'Chronic diastolic heart failure']

dictt = {'Hemiparesis': 'G81.90', 'Ovarian Cancer': 'C56.9',
         'Leg swelling': 'R22.43', 'fluttering/palpitations': 'R00.2', 'Aortic anerysm': 'I71.9',
         'TIA': 'G45.9', 'Presbyopia': 'H52.4',
         'Mitral valve disorder': 'I34.9', 'CAD Angina': 'I25.119', 'hypertension': 'I11.9',
         'Aortic sclerosis': 'I70.0', 'Fatigue': 'R53.83', 'Attention and concentration deficit': 'R41.840',
         'Asymmetry of optic nerve of both eyes': 'H47.093', 'Osteopenia': 'M85.8', 'CHF': 'I50.9',
         'Gastroesophageal reflx disease': 'K21.9', 'Weight Loss': 'R63.4', 'Hearing loss': 'H91.90',
         'Carotid Stenosis': 'I65.29', 'Interstitial Lng disease': 'J84.9', 'Esophagitis esophagitis': 'K20.0',
         'Dizziness': 'R42', 'Age-related nuclear cataract of both eyes': 'H25.13',
         'Abbnormal blood sugar': 'R73.09', 'Thyromegaly': 'E01.0', 'Cardiomyopathy nspecified': 'I42.9',
         'hemoptysis': 'R04.2', 'Thyroid disorder': 'E07.9', 'Vitamin B12 deficiency': 'D51.0',
         'Chronic diastolic heart failre': 'I50.32', 'First degree hemorrholds': 'K64.0',
         'Hyperopia': 'H52.00', 'Rhematoid arthritis with rhematoid factor': 'M05.9', 'dyslipidemia': 'E78.5',
         'eye disorder': 'H57.9', 'dysuria': 'R30.0', 'Kidney stone': 'N20.0', 'Plmonary fibrosis': 'J84.10',
         'Encounter for screening for other disorder': 'Z13.89', 'migraines': 'G43.909',
         'Personal history of nicotine dependence': 'Z87.891',
         'AML': 'C92.00',
         'Atrial fibrillation': 'I48.91', 'hyperlipidemia': 'E78.2', 'diabetes': 'E11.9',
         'Chest Pain': 'R07.9', 'Myocardial infarction': 'I25.2', 'Nasal congestion': 'R09.81',
         'cough': 'J45.991', 'cataracts': 'H26.9', 'Chronic systolic congestive heart failre': 'I50.22',
         'Rhematoid Arthritis': 'M06.9', 'Paroxysmal Atrial fibrillation': 'I48.0', 'conjunctiva': 'H11.89',
         'Acte on chronic diastolic heart failre': 'I50.33', 'photopsias': 'H53.19', 'Genitourinary': 'R39.15',
         'Iron deficiency anemia': 'D50.9',
         'Sleep disorder': 'G47.9', 'AKI': 'N17.9', 'Mild episode of recrrent major depressive disorder': 'F33.0',
         'New palpable lump left breast': 'N63.20', 'AIDS': 'B20', 'Inslin': 'Z79.4', 'Leiomyoma of uterus': 'D25.1',
         'dyspnea': 'R06.02', 'Morbidly obese': 'E66.01', 'hepatomegaly': 'R16.0',
         'diarrhea-predominant (IBS-D)': 'K58.0', 'Colon cancer': 'C18.9', 'Angio-edema': 'L50', 'PVD': 'I73.9',
         'Floater': 'H43.393', 'diastolic heart failre Acte': 'I50.31', 'wheezing': 'R06.2', 'Andonimal Pain': 'R10.9',
         'Malignant neoplasm of upper-outer quadrant of left female breast': 'C50.412', 'metastatic disease': 'C79.9',
         'Glaucoma': 'H40', 'ADD': 'F90.9', 'pachymeningitis': 'G03.9',
         'Primary open angle glaucoma of both eyes': 'H40.1131',
         'Chronic thrombocytopenia': 'D69.6', 'Cancer': 'C80.1', 'Hypercholesterolemia': 'E78.00',
         'oropharynx and tonsils ': 'J35.8', 'Diverticulosls (No Bleed)': 'K57.5', 'Snores': 'R06.83',
         'Headache Primary': 'G44.53', 'spontaneous': 'R23.3', 'Asthma': 'J45.909',
         'Systolic congestive heart failre': 'I50.20', 'Heart failre Diastolic dysfnction': 'I50.30',
         'Vitamin D deficiency': 'E55.9', 'Pancreatic Cancer': 'C25.9',
         'Breast cancer': 'C50.919', 'CAD': 'I25.10', 'h/o iron defficiency ': 'E66.1',
         'Rhematoid arthritis with rhematoid factor of mltiple sites withot organ or systems involvement': 'M05.79',
         'HIV': 'Z21', 'BMI 45': 'Z68.42', 'Arthritis': 'M13.80', 'diarrhea': 'R19.7', 'Sleep apnea': 'G47.30',
         'Cardiac arrest': 'I46.9', 'hematuria': 'R31', 'UTI symptoms': 'N39.0',
         'Stage I right breast cancer': 'C50.911', 'Intracranial hypotension': 'G97.2',
         'Depression': 'F32.9', 'Acute maxillary sinusitis': 'J01.00', 'Anerysm': 'I72.9',
         'Prostatitis ': 'N41.0', 'Gallbladder polyp': 'K82.4', 'Constipation': 'K59.00', 'Urticaria': 'L50.9',
         'Stroke': 'I63.9', 'Changes in retinal vascular appearance of both eyes': 'H35.013', 'Angina': 'I20.9',
         'peripheral lymphadenopathy': 'A18.2', 'Dementia': 'F03.90', 'APPENDECTOMY': 'K35',
         'High Blood Pressure': ' Z01.30'}

new_dict = dict((k.upper(), v.upper()) for k, v in dictt.items())


lookup_list = [i.lower() for i in lookup_list]


def text_genrator(text):
    new_text = text.split('\n')
    new_text = [' '.join(tokenizer.tokenize(i.lower())) for i in new_text if i != '' and i not in string.punctuation]
    edit_distance_lst = []
    combi_steps = list(itertools.product(lookup_list, new_text))
    #     combi_steps
    for i in combi_steps:
        for j in i[1].split():
            if len(j) <= 3:
                if nltk.edit_distance(j, i[0]) < 1:
                    edit_distance_lst.append(i[1].replace(j, i[0]))
            if 3 < len(j) <= 6:
                if nltk.edit_distance(j, i[0]) < 2:
                    edit_distance_lst.append(i[1].replace(j, i[0]))

            if len(j) > 6:
                if nltk.edit_distance(j, i[0]) < 3:
                    edit_distance_lst.append(i[1].replace(j, i[0]))

    #     edit_distance_lst
    new_text1 = [i for i in edit_distance_lst for j in i.split() if j in lookup_list]
    new_text2 = [' '.join(j for j in i.split() if j.isalpha()) for i in new_text1]
    return new_text2

